#include <stdbool.h>

typedef struct config {
        bool force_ipv4;
        bool force_ipv6;
        bool dnssec;
        char *dnsrec;
        char *capath;
        char *url;    
} config;

bool	 parse_config(char *, config *);
void	 free_config_struct(config *);
