#include <sys/cdefs.h>
__FBSDID("$FREEBSD$");

#include <libxo/xo.h>

#include <paths.h>
#include <unistd.h>

#include "hbsd-update.h"
#include "config.h"
#include "utils/crypto.h"
#include "utils/netutils.h"
#include "utils/strutils.h"
#include "utils/ioutils.h"

bool
verify_download(const char *download_dir, struct options opts) {
	struct pubkey pubkey;
	int number_of_files = 8;
	char files[][MAXPATHLEN] = {
		"kernel-HARDENEDBSD.txz",
		"mtree.tar",
		"skip.txt",
		"src.txz",
		"base.txz",
		"ObsoleteDirs.txt",
		"ObsoleteFiles.txt",
		"etcupdate.tbz"
	};
	char file_location[MAXPATHLEN], sig_location[MAXPATHLEN];
	bool status = false;
	int err;

	if (opts.verbose_output)
		fprintf(stderr, "[*] Verifying download's signature\n");

	sprintf(file_location, "%s/pubkey.pem", download_dir);
	pubkey = extract_pubkey(file_location);
	ERR_CHECK2(pubkey.key, "[-] Failed extracting public key!\n");

	for (int i = 0; i < number_of_files; i++) {
		sprintf(file_location, "%s/%s", download_dir, files[i]);
		sprintf(sig_location, "%s/%s.sig", download_dir, files[i]);
		if ((err = verify_signature(pubkey, sig_location, file_location)) != 1) {
			fprintf(stderr, "[-] Failed verifying %s!\n", files[i]);
			goto end;
		}
		if (opts.verbose_output)
			fprintf(stderr, "[+] %s is OK\n", files[i]);
	}

	status = true;
end:
	free(pubkey.key);
	return status;
}

void
print_version_json(hbsd_version local, hbsd_version remote){
        xo_set_options(NULL, "json,pretty");

        xo_open_container("local");
        xo_emit("{:fullstr/%s}", local.fullstr);
        xo_emit("{:src_str/%s}", local.srcver);
        xo_emit("{:hbsd_ver/%s}", local.hbsdver);
        xo_close_container("local");

        xo_open_container("remote");
        xo_emit("{:fullstr/%s}", remote.fullstr);
        xo_emit("{:src_str/%s}", remote.srcver);
        xo_emit("{:hbsd_ver/%s}", remote.hbsdver);
        xo_open_container("hash");
        xo_emit("{:algorithm/%s}", remote.hash_algo);
        xo_emit("{:hash/%s}", remote.hash);
        xo_close_container("hash");
        xo_close_container("remote");

        xo_finish();   
}

/* Check the latest version of HardenedBSD */
bool
check_remote_version(hbsd_version *remote, struct options opts) {
        char *buffer = NULL;
	bool stat = false;
        if (!opts.disable_dnssec) {
                buffer = dnssec_check(opts);
                if (buffer == NULL)
			goto end;
        } else {
                char url[MAXHOSTNAMELEN];
                char location[MAXPATHLEN];

                /* Fetch the content of update-latest.txt */
                sprintf(url, "%s/update-latest.txt", opts.baseurl);
                sprintf(location, "%s/update-latest.txt", opts.tmp_dir);

                if (!fetch(url, location, opts, 1))
			goto end;
                FILE *file = fopen(location, "r");
                if (file == NULL) {
                        fprintf(stderr, "[-] IO Error: %s\n", strerror(errno));
                        goto end;
                }
		
		size_t n;
		getline(&buffer, &n, file);
                fclose(file);
		if (buffer == NULL) {
			fprintf(stderr, "[-] Error: failed to read %s\n", location);
			goto end;
		}
        }
	
	if (!remove_unprintable_characters(buffer)) {
		fprintf(stderr, "[-] Error: remove_unprintable_characters() failed!\n");
		goto end;
	}

        /* Parse latest-update.txt */
	char *token, delim[] = "|:";
	token = strtok(buffer, delim);
	remote->timestamp = strtol(token, NULL, 10);
	if (errno == ERANGE || remote->timestamp == 0)
		goto end;
	token = strtok(NULL, delim);
	if (token == NULL)
		goto end;
	STRDUP_CHECKED(remote->fullstr, token, false);
	token = strtok(NULL, delim);
	if (token == NULL)
		goto end;
	STRDUP_CHECKED(remote->hash_algo, token, false);
	token = strtok(NULL, delim);
	if (token == NULL)
		goto end;
	STRDUP_CHECKED(remote->hash, token, false);

	if (strcasecmp(remote->hash_algo, "sha256") != 0 &&
	    strcasecmp(remote->hash_algo, "sha512") != 0) {
		fprintf(stderr, "[-] Error: \"%s\" hashing algorithm is unsupported!\n",
		    remote->hash_algo);
		goto end; 
	}

        /* Parse HardenedBSD and Source versions */
	if (parse_hbsdversion(remote->fullstr, remote))
		stat = true;

end:
	free(buffer);
        remote->stat = stat;
        return stat;
}

/* Check installed version of HardenedBSD */
bool
check_local_version(hbsd_version *local, struct options opts){
        /* Read the content of /var/db/hbsd-update/version */
        char flocation[MAXPATHLEN];
        sprintf(flocation, "%s/var/db/hbsd-update/version", opts.mountpoint);
        FILE *file = fopen(flocation, "r");
	int err, stat = 0;

	if (file == NULL)
                goto end;

	size_t s;
        err = getline(&local->fullstr, &s, file);
	if (err == -1) {
		fprintf(stderr, "[-] Error: can't read %s\n", flocation);
		goto end;
	}

        fclose(file);

        if (!remove_unprintable_characters(local->fullstr))
		goto end;

        /* Parse HardenedBSD and Source versions */
	if (!parse_hbsdversion(local->fullstr, local))
		fprintf(stderr, "[-] Parsing error: failed parsing %s content\n"
		    ,flocation);
	stat = true;	
end:
        local->stat = stat;
        return stat;
}

void
free_options_struct(struct options *opts) {
	free(opts->kernel);
	free(opts->mountpoint);
	free(opts->jail_name);
	free(opts->version);
	free(opts->be_name);
	free(opts->config_location);
	free(opts->tmp_dir);
	free(opts->backup_kernel);
}

void
free_hbsdversion_struct(struct hbsd_version *hbsdver) {
	free(hbsdver->fullstr);
	free(hbsdver->hbsdver);
	free(hbsdver->srcver);
	free(hbsdver->hash);
	free(hbsdver->hash_algo);
}

void
usage() {
        fprintf(stderr, "USAGE: hbsd-update OPTIONS\n");
        fprintf(stderr, "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s",
                        "\t-4\t\tForce IPv4\n", "\t-6\t\tForce IPv6\n",
                        "\t-b BEname\tInstall updates to ZFS Boot Environment <BEname>\n",
                        "\t-C\t\tCheck the current local and remote version\n",
                        "\t-c config\t\tUse a non-default config file\n",
                        "\t-d\tDo not use DNSSEC validation\n", "\t-f\t\tFetch only\n",
                        "\t-F\t\tDownload only\n", "\t-h\t\tShow this help screen\n",
                        "\t-I\t\tInteractively remove obsolete files\n",
                        "\t-i\t\tIgnore version check\n",
                        "\t-j jailname\tInstall updates to jail <jailname>\n",
                        "\t-K backupkern\tBackup the previous kernel to <backupkern>\n",
                        "\t-k kernel\tUse kernel <kernel>\n",
                        "\t-m\t\tJSON output of version (requires -C)\n",
                        "\t-n\t\tDo not install kernel\n",
                        "\t-o\t\tDo not remove obsolete files/directories\n",
                        "\t-R\t\tUse system nameserver for the DNS-based version check\n",
                        "\t-r path\t\tBootstrap root directory <path>\n",
                        "\t-s\t\tInstall sources (if present)\n",
                        "\t-t tmpdir\tTemporary directory (example: /root/tmp)\n",
                        "\t-U\t\tAllow unsigned updates\n",
                        "\t-v version\tUse a different version\n",
                        "\t-V\t\tVerbose output\n");
}

int
main(int argc, char **argv) {
        config conf;
	struct options opts;
	char *hash = NULL;
	int status = EXIT_SUCCESS, c;

	memset(&opts, 0, sizeof(struct options));
        while ((c = getopt(argc, argv,
	    "46b:Cc:dfFhIij:K:k:mnoRr:st:Uv:V")) != -1)
                switch (c) {
                case '4':
                        opts.force_ipv4 = true;
                        break;
                case '6':
                        opts.force_ipv6 = true;
                        break;
                case 'b':
			fprintf(stderr, "[-] Warning: -%c is not implemented yet\n", c);
			STRDUP_CHECKED(opts.be_name, optarg, EXIT_FAILURE);
                        break;
                case 'C':
			opts.check_version_only = true;
                        break;
                case 'c':
			STRDUP_CHECKED(opts.config_location, optarg, EXIT_FAILURE);
                        break;
                case 'd':
			opts.disable_dnssec = true;
                        break;
                case 'f':
			opts.fetch_only = true;
                        break;
                case 'F':
                        opts.download_only = true;
			break;
                case 'I':
			fprintf(stderr, "[-] Warning: -%c is not implemented yet\n", c);
                        break;
                case 'i':
			fprintf(stderr, "[-] Warning: -%c is not implemented yet\n", c);
                        break;
                case 'j':
			fprintf(stderr, "[-] Warning: -%c is not implemented yet\n", c);
                        STRDUP_CHECKED(opts.jail_name, optarg, EXIT_FAILURE);
                        break;
                case 'K':
			fprintf(stderr, "[-] Warning: -%c is not implemented yet\n", c);
			STRDUP_CHECKED(opts.backup_kernel, optarg, EXIT_FAILURE);
			break;
                case 'k':
			fprintf(stderr, "[-] Warning: -%c is not implemented yet\n", c);
			STRDUP_CHECKED(opts.kernel, optarg, EXIT_FAILURE);
                        break;
                case 'm':
			opts.json_version_output = true;
                        break;
                case 'n':
			fprintf(stderr, "[-] Warning: -%c is not implemented yet\n", c);
                        break;
                case 'o':
			fprintf(stderr, "[-] Warning: -%c is not implemented yet\n", c);
                        break;
                case 'R':
			opts.use_system_nameserver = true;
                        break;
                case 'r':
			fprintf(stderr, "[-] Warning: -%c is not implemented yet\n", c);
			STRDUP_CHECKED(opts.mountpoint, optarg, EXIT_FAILURE);
                        break;
                case 's':
			fprintf(stderr, "[-] Warning: -%c is not implemented yet\n", c);
                        break;
                case 't':
			fprintf(stderr, "[-] Warning: -%c is not implemented yet\n", c);
			STRDUP_CHECKED(opts.tmp_dir, optarg, EXIT_FAILURE);
                        break;
                case 'U':
                        opts.allow_unsigned_updates = true;
                        break;
                case 'v':
			STRDUP_CHECKED(opts.version, optarg, EXIT_FAILURE);
                        break;
                case 'V':
                        opts.verbose_output = true;
                        break;
                default:
                        fprintf(stderr, "[-] Illegal option -%c\n", c);
                case '?':
                case 'h':
                        usage();
                        exit(1);
                }
        
        argc -= optind;
        argv += optind;

	if (opts.mountpoint == NULL)
		STRDUP_CHECKED(opts.mountpoint, "/", EXIT_FAILURE);
	if (opts.tmp_dir == NULL)
		STRDUP_CHECKED(opts.tmp_dir, "/tmp", EXIT_FAILURE);
	if (opts.config_location == NULL)
		STRDUP_CHECKED(opts.config_location,
		    "/etc/hbsd-update.conf", EXIT_FAILURE);

        /* Parsing Configuration */   
        if (!parse_config(opts.config_location, &conf)) {
                status=EXIT_FAILURE;
		goto end;
	}

        if (conf.force_ipv4)
		opts.force_ipv4 = true;
        if (conf.force_ipv6)
		opts.force_ipv6 = true;

        if (conf.dnssec && !opts.disable_dnssec) 
		opts.dnsrec = conf.dnsrec;
        if (conf.capath)
		opts.capath = conf.capath;
        if (!conf.url) {
                fprintf(stderr, "[-] Configuration Error: URL not found!\n");
                status = EXIT_FAILURE;
		goto end;
        } else
		opts.baseurl = conf.url;

        if (opts.force_ipv4 && opts.force_ipv6) {
                fprintf(stderr, "[-] force_ipv6 is mutually exclusive with force_ipv4\n");
                status = EXIT_FAILURE;
		goto end;
        }

        /* Check Installed HardenedBSD version */
        hbsd_version local;
        if(!check_local_version(&local, opts))
                fprintf(stderr, "[-] Warning: Cannot detect HardenedBSD version on this system!\n");

	hbsd_version remote;
	if (opts.version != NULL)
		remote.fullstr = opts.version; /* if -v was present use specified version */
	else if (!check_remote_version(&remote, opts)) { /* if not check for the latest version */
		fprintf(stderr, "[-] Checking remote version failed!\n");
		status = EXIT_FAILURE;
		goto end;
	}
	
	if (remote.hash == NULL && !opts.allow_unsigned_updates) {
		fprintf(stderr, "[-] Hash for the update has not been published. Cannot verify update.\n");
		status = EXIT_FAILURE;
		goto end;
	}

	/* Display the current local and remote version when using -C */
        if (opts.check_version_only) {
                /* If -m option was present use JSON format */
                if (opts.json_version_output)
			print_version_json(local, remote);
                else {
                        if (local.stat)
				printf("[+] Local version: %s\n", local.fullstr);
                        printf("[+] Remote version: %s %s %s\n", remote.fullstr, 
                                        remote.hash_algo, remote.hash);
		}
		goto end;
	}

	/* Check if the system is up to date */
	if (local.stat && !strcmp(remote.fullstr, local.fullstr) && !opts.download_only) {
		printf("[*] Your system is up-to-date!\n");
		goto end;
	}

	if (!isroot() && !opts.download_only && !opts.fetch_only) {
		fprintf(stderr, "[-] Error: root permission required!\n");
		goto end;
	}

	/* Download the update */
	char url[MAXHOSTNAMELEN], location[MAXPATHLEN];
	sprintf(location, "%s/update.tar", opts.tmp_dir);
	sprintf(url, "%s/update-%s.tar", opts.baseurl, remote.fullstr);
download:
	if (!fetch(url, location, opts, 1)) {
		status = EXIT_FAILURE;
		goto end;
	}

	/* Checking download's hash */
	if (!opts.allow_unsigned_updates) {
		printf("[*] Verifying download's integrety...\n");
		if (strcasecmp(remote.hash_algo, "sha256") == 0)
			hash = sha256sum(location);
		else if (strcasecmp(remote.hash_algo, "sha512") == 0)
			hash = sha512sum(location);
		if (strcasecmp(hash, remote.hash) != 0) {
			fprintf(stderr, "\n[-] Error: hash mismatch!\n");
			free(hash);
			hash = NULL;
			goto download;
		} else if (opts.verbose_output)
			fprintf(stderr, "[+] Update's hash is valid.\n");
	}

	/* Extract "update.tar" */
	if (opts.verbose_output)
		fprintf(stderr, "[*] Extracting archive.tar\n");
	if (!archive_extract(location, opts.tmp_dir, opts)) {
		fprintf(stderr, "[-] Extracting failed!\n");
		status = EXIT_FAILURE;
		goto end;
	}

	if (!opts.allow_unsigned_updates) {
		if (!verify_download("/tmp", opts)) {
			status = EXIT_FAILURE;
			goto end;
		}
	}

end:
	free_config_struct(&conf);
	free_options_struct(&opts);
	free_hbsdversion_struct(&local);
	free_hbsdversion_struct(&remote);
	free(hash);
	return status;
}
