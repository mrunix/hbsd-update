PACKAGE=	hbsd-update

PROG=		hbsd-update
SRCS=		hbsd-update.c config.c utils/*.c
								
CFLAGS+=	-g -fPIC -fPIE -fsanitize=safe-stack -fsanitize=cfi\
		-fvisibility=hidden -flto  -mretpoline -Wall -Werror\
		-Wno-format-zero-length -fstack-protector-strong\
		-Wsystem-headers -Wno-unused-variable -Wpointer-arith\
		-Wreturn-type -Wcast-qual -Wwrite-strings -Wswitch\
		-Wshadow -Wcast-align -Wchar-subscripts -Wnested-externs\
		-Wthread-safety -O0
INCLUDE+=	-I/usr/src/contrib/libucl/include -I/usr/src/contrib/unbound/libunbound
LIBS+=		-lfetch -lcrypto -lprivateucl -lxo -lprivateunbound -larchive

all:		$(SRCS)
	$(CC) $(CFLAGS) $(INCLUDE) $(LIBS) -o $(PROG) $(SRCS)
clean:
	rm -f $(PROG) *core vgcore*
