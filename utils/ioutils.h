#include <archive.h>
#include <archive_entry.h>
#include <errno.h>

#include "misc.h"
#ifndef STRDUP_CHECKED
#include "../hbsd-update.h"
#endif

struct file {
	unsigned char	*data;
	size_t		 size;
};

bool		 archive_extract(char *archive, char *dest, struct options opts);
struct file	 read_file(char *location);
