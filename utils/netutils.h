#include <paths.h>

#ifndef STRDUP_CHECKED
#include "../hbsd-update.h"
#endif

bool     fetch(char *url, char *location, struct options opts, bool show_progress);
char    *dnssec_check(struct options opts);
