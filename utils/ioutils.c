#include "ioutils.h"

#define ARCHIVE_ERR_CHECK(err)	if (err != ARCHIVE_OK) {        \
                fprintf(stderr, "[-] Extracting error: %s\n",	\
                    archive_error_string(a));			\
        goto end;					        \
}

bool
archive_extract(char *archive, char *dest, struct options opts) {
        struct archive *a;
        struct archive_entry *e;
        int err;
        bool status = false;

        a = archive_read_new();
        if (a == NULL) {
                fprintf(stderr, "[-] Error: archive_read_new() failed!\n");
                goto end;
        }

        err = archive_read_support_format_all(a);
        ARCHIVE_ERR_CHECK(err);

        err = archive_read_open_filename(a, archive, 10240);
        ARCHIVE_ERR_CHECK(err);

        while (archive_read_next_header(a, &e) == ARCHIVE_OK) {
                char path[MAXPATHLEN];
                const char *archive_pathname = archive_entry_pathname(e);

                /* Skip trailing "./" if it exists */
                if(archive_pathname[0] == '.' && archive_pathname[1] == '/')
                        archive_pathname += 2;

                snprintf(path, sizeof(path), "%s/%s", dest, archive_pathname);

                /* If the current header is a directory, create it */
                if (archive_entry_filetype(e) == AE_IFDIR) {
                        if (opts.verbose_output)
                                fprintf(stderr, "[*] Creating directory: \"%s\"\n", path);
                        if (mkdir(path, 0640) != 0 && errno != EEXIST) {
                                if (errno == EEXIST && opts.verbose_output)
                                        fprintf(stderr, "[*] %s already exists", path);
                                else {
                                        fprintf(stderr, "[-] Can't create %s: %s\n", path ,strerror(errno));
                                        goto end;
                                }
                        }
                        continue;
                }

                FILE *fp = fopen(path, "wb");
                if (fp == NULL) {
                        fprintf(stderr, "[-] Can't create %s: %s\n",path ,strerror(errno));
                        goto end;
                }

                if (opts.verbose_output)
                        fprintf(stderr, "[*] Extracting %s\n", path);

                const void* buff;
                size_t size;
                off_t offset;
                while ((err = archive_read_data_block(a, &buff, &size, &offset)) != ARCHIVE_EOF) {
                        if (err != ARCHIVE_OK) {
                                fprintf(stderr, "[-] Failed extracting %s: %s\n",
                                        archive_entry_pathname(e), archive_error_string(a));
                                fclose(fp);
                                goto end;
                        }
                        fwrite(buff, size, 1, fp);
                }
                fclose(fp);
                fp = NULL;
        }
        status = true;
end:
        archive_read_close(a);
        archive_read_free(a);
        return status;
}

struct file
read_file(char *location) {
        FILE *fp = NULL;
        struct file f;
        size_t offset = 0;
        memset(&f, 0, sizeof(struct file));

        fp = fopen(location, "rb");
        ERR_CHECK(fp, "[-] IO Error");

        do {
                f.data = realloc(f.data, (offset + 1024) * sizeof(unsigned char));
                ERR_CHECK2(f.data, "[-] Memory allocation failed!\n");
                f.size += fread(f.data + offset, 1, 1024, fp);
                offset += 1024;
                if (ferror(fp)) {
                        perror("[-] IO Error:");
                        goto end;
                }
        } while (!feof(fp));
end:
        fclose(fp);
        return f;
}
