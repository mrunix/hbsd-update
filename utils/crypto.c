#include "crypto.h"
#include "ioutils.h"

char* 
sha256sum(char *file){
        char buffer[4096];
        FILE *fp = fopen(file, "rb");
        if (fp == NULL)
                return NULL;

        SHA256_CTX ctx;
        SHA256_Init(&ctx);

        size_t bytes_read;
        while ((bytes_read = fread(buffer, 1, sizeof(buffer), fp)) != 0)
                SHA256_Update(&ctx, buffer, bytes_read);

        unsigned char hash[SHA256_DIGEST_LENGTH];
        SHA256_Final(hash, &ctx);
        fclose(fp);

        return hash_to_string(hash, SHA256_DIGEST_LENGTH);
}

char*
sha512sum(char *file){
        char buffer[4096];

        FILE *fp = fopen(file, "rb");
        if (fp == NULL)
                return NULL;

        SHA512_CTX ctx;
        SHA512_Init(&ctx);

        size_t bytes_read;
        while ((bytes_read = fread(buffer, 1, sizeof(buffer), fp)) != 0)
                SHA512_Update(&ctx, buffer, bytes_read);

        unsigned char hash[SHA512_DIGEST_LENGTH];
        SHA512_Final(hash, &ctx);
        fclose(fp);
        return hash_to_string(hash, SHA512_DIGEST_LENGTH);
}

char*
hash_to_string(unsigned char *hash, size_t size){
	char *string = malloc(size * 2 + 1);
        if (string == NULL || hash == NULL)
                return NULL;
        string[0] = '\0';
        char buff[3];
        for (uint8_t i = 0; i < size; i++){
                sprintf(buff, "%02x", hash[i]);
                strcat(string, buff);
        }
        return string;
}

struct pubkey
extract_pubkey(char *cert_location) {
	BIO *cert_bio = NULL, *pubkey_bio = NULL;
	X509 *cert = NULL;
	EVP_PKEY *pkey = NULL;
	BUF_MEM *bptr = NULL;

	struct pubkey output;
	memset(&output, 0, sizeof(struct pubkey));

	cert_bio = BIO_new_file(cert_location, "r");
	ERR_CHECK(cert_bio, "[-] BIO_new_file()");

	cert = PEM_read_bio_X509(cert_bio, NULL, NULL, NULL);
	ERR_CHECK(cert, "[-] PEM_read_bio_x509() failed");

	pkey = X509_get_pubkey(cert);
	ERR_CHECK(pkey, "[-] X509_get_pubkey() failed");

	pubkey_bio = BIO_new(BIO_s_mem());
	ERR_CHECK(pubkey_bio, "[-] BIO_new() failed");

	if (!PEM_write_bio_PUBKEY(pubkey_bio, pkey)) {
		perror("[-] PEM_write_bio_PUBKEY failed");
		goto end;
	}

	BIO_get_mem_ptr(pubkey_bio, &bptr);
	output.key = (char*) malloc(bptr->length + 1);
	ERR_CHECK2(output.key, "[-] Memory allocation failed!\n");

	memcpy(output.key, bptr->data, bptr->length);
	output.key[bptr->length] = '\0';
	output.len = bptr->length + 1;

end:
	BIO_free(cert_bio);
	BIO_free(pubkey_bio);
	EVP_PKEY_free(pkey);
	X509_free(cert);
	return output;
}

bool
verify_signature(struct pubkey pubkey, char* signature_path, char *file) {
	RSA *rsa = NULL;
	BIO *pubkey_bio = NULL;
	struct file signature;
	char *digest = NULL, *hash = NULL;
	bool status = false;

	/* Read the signature from "signature_path" file */
	signature = read_file(signature_path);
	if (signature.data == NULL || signature.size <= 0)
		goto end;

	pubkey_bio = BIO_new_mem_buf(pubkey.key, pubkey.len);
	ERR_CHECK2(pubkey_bio, "[-] Memory allocation failed!\n");
	rsa = PEM_read_bio_RSA_PUBKEY(pubkey_bio, NULL, NULL, NULL);
	ERR_CHECK2(rsa, "[-] Memory allocation failed!\n");

	digest = malloc(SHA512_DIGEST_LENGTH * 2 + 1);
	ERR_CHECK2(digest, "[-] Memory allocation failed!\n");

	if (RSA_public_decrypt(RSA_size(rsa), (unsigned char*) signature.data,
	    (unsigned char*) digest, rsa, RSA_PKCS1_PADDING) == -1)
		goto end;
	digest[SHA512_DIGEST_LENGTH*2] = '\0';

	if (digest == NULL)
		goto end;

	hash = sha512sum(file);
	ERR_CHECK2(hash, "[-] sha512sum() failed!\n");

	if (strcmp(hash, digest) == 0)
		status = true;

end:
	RSA_free(rsa);
	BIO_free(pubkey_bio);
	free(digest);
	free(hash);
	free(signature.data);
	return status;
}
