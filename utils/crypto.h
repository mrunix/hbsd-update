#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/pem.h>
#include <openssl/sha.h>

#include "misc.h"

struct pubkey {
	char	*key;
	size_t	 len;
};

char    	*sha256sum(char *file);
char    	*sha512sum(char *file);
char    	*hash_to_string(unsigned char *hash, size_t size);

struct pubkey	 extract_pubkey(char *cert_location);
bool	 	 verify_signature(struct pubkey, char *signature_path, char *file);
