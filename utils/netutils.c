#include <sys/cdefs.h>
__FBSDID("$FreeBSD#");

#include <sys/param.h>

#include <stdio.h>
#include <fetch.h>
#include <unbound.h>

#include "netutils.h"
#include "strutils.h"

/* Download a file from "url" and save it to "location" */
bool 
fetch(char *url, char *location, struct options opts, bool show_progress){
        struct url_stat url_info;
        char flags[2] = "";
        if (opts.force_ipv4)
                strcpy(flags, "4");
        else if (opts.force_ipv6)
                strcpy(flags, "6");
        FILE *remote = fetchXGetURL(url, &url_info, flags);
        if (remote == NULL) {
                fprintf(stderr, "[-] Could not fetch \"%s\": %s\n",url ,fetchLastErrString);
                return false;
        }

        FILE *output;
download:
        output = fopen(location, "wb");
        if (output == NULL) {
                fprintf(stderr, "[-] Can't access %s\n", location);
                return false;
        }

        char buff[10240];
        off_t n, downloaded = 0;

        if(show_progress)
                printf("[*] Downloading \"%s\"\n", url);

        while (1) {
                n = fread(buff, 1, sizeof(buff), remote);
                if (n == 0)
                        break;
                downloaded += fwrite(buff, 1, n, output);

                if (show_progress) {
                        double percentage = (double)downloaded / url_info.size * 100;
                        printf("\r[%.lf%%] %s (%.1lf/%.1lf MiB)", percentage, location,
                                        (double)downloaded / 1048576, (double)url_info.size / 1048576);
                        fflush(stdout);
                }
        }
        if (show_progress)
                putchar('\n');

        if (downloaded != url_info.size) {
                fprintf(stderr, "[-] File size mismatch, redownloading...\n");
                fclose(output);
                goto download;
        }
        fclose(remote);
        fclose(output);
        return true;
}

char*
dnssec_check(struct options opts) {
        struct ub_ctx *ctx = ub_ctx_create();
        char dnssec_key[MAXPATHLEN];
        char *txt_record = NULL;
        int err;

        sprintf(dnssec_key, "%s/dnssec.key", opts.capath);
        err = ub_ctx_add_ta_file(ctx, dnssec_key);
        if (err) {
                fprintf(stderr, "[-] DNSSEC failed: %s\n", ub_strerror(err));
                goto error;
        }

        if (opts.use_system_nameserver) {
                err = ub_ctx_resolvconf(ctx, NULL);
                if (err)
                        goto error;
        }

        if (opts.force_ipv4) {
                err = ub_ctx_set_option(ctx, "do-ip6", "no");
                if (err)
                        goto error;
        } else if (opts.force_ipv6) {
                err = ub_ctx_set_option(ctx, "do-ip4", "no");
                if (err)
                        goto error;
        }

        struct ub_result *result = NULL;
        err = ub_resolve(ctx, opts.dnsrec, 16, 1, &result);
        if (err) {
                fprintf(stderr, "[-] DNSSEC failed: Failed to resolve DNS query: %s.\n", ub_strerror(err));
                goto error;
        }

        if (result->havedata && result->secure && result->len) {
                txt_record = malloc(*result->len + 1);
		memcpy(txt_record, *result->data, *result->len);
		txt_record[*result->len] = '\0';
		if (txt_record == NULL) {
			fprintf(stderr, "[-] Error: memory allocation failed!\n");
			goto error;
		}
	} else
		goto error;
        ub_ctx_delete(ctx);
        ub_resolve_free(result);
        remove_unprintable_characters(txt_record);
        return txt_record;

error:
        fprintf(stderr, "[-] DNSSEC Error: Could not get DNS record.\n");
        ub_ctx_delete(ctx);
        return txt_record;
}
