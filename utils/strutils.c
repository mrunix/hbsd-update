#include "strutils.h"

bool
remove_unprintable_characters(char *input) {
        if (input == NULL)
                return false;
	
        size_t j = 0, n = strlen(input);
        char *output = malloc(n+1);
        if (output == NULL){
		fprintf(stderr, "[-] Error: memory allocation failed!\n");
                return false;
	}
        for (size_t i = 0; i < n; i++){
                if (isprint(input[i]))
                        output[j++] = input[i];
        }
        output[j] = '\0';
        strcpy(input, output);
	free(output);
        return true;
}

bool
parse_hbsdversion(char *str, hbsd_version *hbsdver) {
	char *token, delim[] = "-";
	char *s = strdup(str);
	token = strtok(s, delim);
	token = strtok(NULL, delim);
	STRDUP_CHECKED(hbsdver->hbsdver, token, false);
	token = strtok(NULL, delim);
	STRDUP_CHECKED(hbsdver->srcver, token, false);
	free(s);
	return true;
}
