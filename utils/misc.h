#include <stdbool.h>

#ifndef STRDUP_CHECKED
#include "../hbsd-update.h"
#endif

#define ERR_CHECK(val, error_message)	\
	if (val == NULL) {		\
		perror(error_message);	\
		goto end;		\
	}

#define ERR_CHECK2(val, error_message)		\
	if (val == NULL) {			\
		fprintf(stderr, error_message);	\
		goto end;			\
	}

bool     isroot();
