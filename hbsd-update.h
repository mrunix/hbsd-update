#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <sys/types.h>
#include <sys/param.h>

#define STRDUP_CHECKED(var, val, rvalue)					\
	var = strdup(val);							\
	if (var == NULL) {							\
		fprintf(stderr, "[-] Error: memory allocation failed\n");	\
		return rvalue;							\
	}

typedef struct options {
        bool     force_ipv4;
        bool     force_ipv6;
        bool     disable_dnssec;
        bool     check_version_only;
        bool     json_version_output;
        bool     fetch_only;
        bool     download_only;
        bool     allow_unsigned_updates;
        bool     use_system_nameserver;
        bool     verbose_output;
        char    *baseurl;
        char    *dnsrec;
        char    *kernel;
        char    *capath;
        char    *mountpoint;
        char    *jail_name;
        char    *version;
        char    *be_name;
        char    *config_location;
        char    *tmp_dir;
        char    *backup_kernel;
} options;

typedef struct hbsd_version{
        char	 *fullstr;
        char	 *srcver;
        char	 *hbsdver;
        char	 *hash_algo;
        char	 *hash;
        uint64_t timestamp;
        bool	 stat;
} hbsd_version;

