#include <sys/cdefs.h>
__FBSDID("$FreeBSD$");

#include <ucl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "config.h"

void
free_config_struct(config *config){
        if (config->url != NULL)
		free(config->url);
        if (config->capath != NULL)
		free(config->capath);
        if (config->dnsrec != NULL)
		free(config->dnsrec);
}

bool
parse_config(char *location, config *config){
        const ucl_object_t *obj, *top;
        struct ucl_parser *parser;
        const char *s;
        bool stat = false;
        memset(config, 0, sizeof(struct config));

        parser = ucl_parser_new(UCL_PARSER_KEY_LOWERCASE);
        if (parser == NULL)
		return stat;

        if (!ucl_parser_add_file(parser, location)){
                fprintf(stderr, "[-] Configuration error: %s not found!\n", location);
                goto end;
        }

        top = ucl_parser_get_object(parser);
        if (top == NULL) {
                fprintf(stderr, "[-] Configuration error: Invalid config!\n");
                goto end;
        }
        obj = ucl_object_find_key(top, "force_ipv4");
        if (obj != NULL)
		config->force_ipv4 = ucl_object_toboolean(obj);

        obj = ucl_object_find_key(top, "force_ipv6");
        if (obj != NULL)
		config->force_ipv6 = ucl_object_toboolean(obj);

        obj = ucl_object_find_key(top, "dnssec");
        if (obj != NULL)
		config->dnssec = ucl_object_toboolean(obj);

        obj = ucl_object_find_key(top, "dnsrec");
        if (obj != NULL) {
                s = ucl_object_tostring(obj);
                if (s != NULL)
			config->dnsrec = strdup(s);
                if (config->dnsrec == NULL)
			goto end;
        }

        obj = ucl_object_find_key(top, "capath");
        if (obj != NULL) {
                s = ucl_object_tostring(obj);
                if (s != NULL)
			config->capath = strdup(s);
                if (config->capath == NULL)
			goto end;
        }

        obj = ucl_object_find_key(top, "url");
        if (obj != NULL) {
                s = ucl_object_tostring(obj);
                if (s != NULL)
			config->url = strdup(s);
                if (config->url == NULL)
			goto end;
        }
        stat = true;

end:
        ucl_parser_free(parser);
        return stat;
}
